@extends('admin.admin.layouts.master_admin')
@section('title','Add_Post')
@section('content')
      <div class="container">
        <div class="container main-content-container  px-4">
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Dashboard</span>
            <h3 class="page-title">post</h3>
          </div>
        </div>
        @foreach($posts as $post)
          <form action="/admin/update/" method="post">
            @csrf
            <div class="row">
              <div class="col">
                <input type="hidden" name="id" value="{{ $post->id }}">
                <div class="form-group">
                  <label>Judul</label>
                  <input type="text" class="form-control" value="{{  $post->judul  }}" name="judul">
                </div>
              </div>
                <div class="col">
                  <div class="form-group">
                    <label>Deskripsi</label>
                    <input type="text" class="form-control" value="{{  $post->deskripsi  }}" name="deskripsi">
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                  <div class="form-group">
                    <label>lain</label>
                    <input type="text" class="form-control" value="{{  $post->lain  }}" name="lain">
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label>Category</label>
                    <select id="inputState" class="form-control" value="{{  $post->kategori  }}" name="Category">
                      <option selected>Choose...</option>
                      <option>Robotika Elektronika</option>
                      <option>Internet of Things</option>
                      <option>Web Dev</option>
                    </select>
                  </div>
                </div>
            </div>
                    <div class="form-group">
                      <label>Gambar</label>
                      <input type="file" class="form-control-file"  value="" name="gambar">
                    </div>
            <button type="submit" class="btn btn-primary" value="update">update</button>
          </form>
      </div>
      @endforeach

@endsection
