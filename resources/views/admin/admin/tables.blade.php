@extends('admin.admin.layouts.master_admin')
@section('title','Tables')
@section('content')
          <div class="main-content-container  px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Tables</h3>
              </div>
            </div>
          </div>
          <div class="container">
              <a class="btn btn-primary mb-2 " href="{{ url('add_post') }}">Tambah Data</a>
            <div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom ">
                      <h6 class="m-0" style="align-items: center">Active Users</h6>
                    </div>
                
                    
      
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-2">No</th>
                          <th scope="col" class="border-2">id</th>
                          <th scope="col" class="border-2">Judul</th>
                          <th scope="col" class="border-2">Deskripsi</th>
                          <th scope="col" class="border-2">lain</th>
                          <th scope="col" class="border-2">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($posts as $post)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $post->id }}</td>
                          <td>{{ $post->judul }}</td>
                          <td>{{ Str::limit($post->deskripsi, 50)}}..</td>
                          <td>{{ Str::limit($post->lain, 50) }}..</td>
                          <td class="d-flex justify-content-center">
                            <a class="btn btn-warning"  href="/admin/edit/{{ $post->id }}">Edit</a>
                            <a class="btn btn-danger ml-3"   href="/admin/hapus/{{ $post->id }}">Hapus</a></td>
                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $posts->links() }}
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection
