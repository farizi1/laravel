@extends('admin.admin.layouts.master_admin')

@section('title','Dashboard')
@section('content')
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Admin</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Posts Aktif</span>
                        <h6 class="stats-small__value count my-3">{{ $posts->count() }}</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <h5>Users</h5>
                        <h6 class="stats-small__value count my-3">20 Users</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </main>
            </div>
</div>

@endsection
