<?php
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\adminpagesController;
use App\Http\Controllers\adminPostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*

Halaman Frontend

*/
Route::get('/', [PagesController::class, 'index']);
Route::get('/', [PostController::class, 'index']);



/*

Halaman backend

*/

Route::get('admin', [PagesController::class,'admin']);
Route::get('/tables', [PagesController::class,'tables']);
Route::get('add_post', [PagesController::class,'post']);
Route::get('add_post', [adminPostController::class, 'index']);
Route::get('tables', [adminPostController::class, 'tables']);


Route::post('/create/store', [adminPostController::class, 'store']);
Route::get('/admin/hapus/{id}', [adminPostController::class, 'destroy']);
Route::get('/admin/edit/{id}', [PagesController::class, 'edit']);
Route::post('/admin/update/', [adminPostController::class, 'update']);




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
