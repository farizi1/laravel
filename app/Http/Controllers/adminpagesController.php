<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminpagesController extends Controller
{
    public function admin()
    {
        return view('admin.admin.index');
    }

    public function post()
    {
        return view('admin.admin.addpost');
    }

    public function tables()
    {
        return view('admin.admin.tables');
    }
}
