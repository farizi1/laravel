<?php

namespace App\Http\Controllers;
use App\Models\post;
use Illuminate\Http\Request;

class adminPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::simplepaginate(3);
        return view('admin.admin.addpost', ['posts' => $post]);
        return view('admin.admin.index', ['posts' => $post]);
       
    }

    public function tables(){
        $post = Post::simplepaginate(5);
        return view('admin.admin.tables', ['posts' => $post]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $post = $request->all();
        Post::create($post);
        session()->flash('success', 'the post was created');
        return redirect()->to('tables');


        // $post = new Post;
        // $post->judul = $request->judul;
        // $post->deskripsi = $request->deskripsi;
        // $post->lain = $request->lain;
        // $post->save();

        // Post::create([
        //     'judul'=> $request->judul,
        //     'deskripsi'=> $request->deskripsi,
        //     'lain'=> $request->lain,
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Post::find($id)->update($request->id)
        ([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'lain' => $request->lain
            ]);
            return redirect('/tables');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->to('tables');

    }
}
