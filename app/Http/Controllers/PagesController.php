<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\post;
class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        return view('welcome');
    }

    public function admin()
    {
        $post = Post::all();
        return view('admin.admin.index', ['posts' => $post]);
    }

    public function post()
    {
        return view('admin.admin.addpost');
    }

    public function tables()
    {
        return view('admin.admin.tables');
    }

    public function edit($id)
    {
        $post = Post::where('id', $id)->get();
        return view('admin.admin.edit', ['posts' => $post]);
    }
}
